#Lab10
#Nama = Avery Charisma Zanathar
#NPM = 2106656283
#Kelas = DDP1
##This code is based on: https://www.geeksforgeeks.org/make-notepad-using-tkinter/ then adapted by: Avery Charisma##

import tkinter
import os
from tkinter import *
from tkinter.messagebox import *
from tkinter.filedialog import *


class Notepad:

    __root = Tk()

    # default window width and height
    __thisWidth = 600
    __thisHeight = 600
    __thisTextArea = Text(__root)
    __thisMenuBar = Frame(__root)

    # To add scrollbar
    __thisScrollBar = Scrollbar(__thisTextArea)
    __file = None

    def __init__(self, **kwargs):

        # Set icon
        try:
            self.__root.wm_iconbitmap("Notepad.ico")
        except:
            pass

        # Set window size (the default is 300x300)

        try:
            self.__thisWidth = kwargs['width']
        except KeyError:
            pass

        try:
            self.__thisHeight = kwargs['height']
        except KeyError:
            pass

        # Set the window text
        self.__root.title("Pacil Editor")

        # Center the window
        screenWidth = self.__root.winfo_screenwidth()
        screenHeight = self.__root.winfo_screenheight()

        # For left-align
        left = (screenWidth / 2) - (self.__thisWidth / 2)

        # For right-align
        top = (screenHeight / 2) - (self.__thisHeight / 2)

        # For top and bottom
        self.__root.geometry('%dx%d+%d+%d' % (self.__thisWidth,
                                              self.__thisHeight,
                                              left, top))

        # To make the textarea auto resizable
        self.__root.grid_rowconfigure(1, weight=1)
        self.__root.grid_columnconfigure(0, weight=1)

        # Add controls (widget)
        self.__thisTextArea.grid(sticky=N + E + S + W, column=0, row=1)

        # To open a already existing file
        self.__openButton = Button(self.__thisMenuBar, text="Open File",
                                   command=self.__openFile)
        self.__openButton.pack(side=LEFT)

        # self.__thisMenuBar.add_command

        # To save current file
        self.__saveButton = Button(self.__thisMenuBar, text="Save File",
                                   command=self.__saveFile)
        self.__saveButton.pack(side=LEFT)

        # To close program
        self.__closeButton = Button(self.__thisMenuBar, text="Quit Program",
                                    command=self.__quitApplication)
        self.__closeButton.pack(side=LEFT)

        self.__thisMenuBar.grid(column=0, row=0, sticky=N+E+W)

        self.__thisScrollBar.pack(side=RIGHT, fill=Y)

        # Scrollbar will adjust automatically according to the content
        self.__thisScrollBar.config(command=self.__thisTextArea.yview)
        self.__thisTextArea.config(yscrollcommand=self.__thisScrollBar.set)

    def __quitApplication(self):
        self.__root.destroy()
        # exit()

    def __openFile(self):

        self.__file = askopenfilename(defaultextension=".txt",
                                      filetypes=[("All Files", "*.*"),
                                                 ("Text Documents", "*.txt")])

        if self.__file == "":

            # no file to open
            self.__file = None
        else:

            # Try to open the file
            # set the window title
            self.__root.title(os.path.basename(self.__file) + " - Pacil Editor")
            self.__thisTextArea.delete(1.0, END)

            file = open(self.__file, "r")

            self.__thisTextArea.insert(1.0, file.read())

            file.close()

    def __newFile(self):
        self.__root.title("- Pacil Editor")
        self.__file = None
        self.__thisTextArea.delete(1.0, END)

    def __saveFile(self):
        self.__file = asksaveasfilename(initialfile='Untitled.txt',
                                        defaultextension=".txt",
                                        filetypes=[("All Files", "*.*"),
                                                   ("Text Documents", "*.txt")])
        file = open(self.__file, "w")
        file.write(self.__thisTextArea.get(1.0, END))
        file.close()
        self.__root.title(os.path.basename(self.__file) + " - Pacil Editor")

    def run(self):

        # Run main application
        self.__root.mainloop()


# Run main application
notepad = Notepad(width=600, height=400)
notepad.run()
