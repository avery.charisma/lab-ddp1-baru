#Lab09#
#Nama:Avery Charisma Zanathar
#NPM: 2106656283
#Kelas: DDP1

import datetime

# class parent karyawan
class Karyawan:
    username = ""
    password = ""
    nama = ""
    umur = 0
    terakhirLogin = ""
    role = ""

    def get_peran(self):
        return self.role

    def set_terakhir_login(self, terakhirLogin):
        self.terakhirLogin = terakhirLogin

    def get_terakhir_login(self):
        return self.terakhirLogin

    def __init__(self, username, password, nama, umur, role):
        self.username = username
        self.password = password
        self.nama = nama
        self.umur = umur
        self.role = role

    def __str__(self) -> str:
        return f"Username: {self.username}\nNama: {self.nama}\nUmur: {self.umur}\nLogin Terakhir: {self.terakhirLogin}\nRole: {self.role}\n"


# class kasir yang merupakan anak dari class karyawan
class Kasir(Karyawan):
    uangDiterima = 0

    def terima_uang(self, uang):
        self.uangDiterima += uang

    def __str__(self) -> str:
        return super().__str__() + f"Jumlah uang diterima: {self.uangDiterima}"


# class janitor yang merupakan anak dari class karyawan
class Janitor(Karyawan):
    jumlahMembersihkanToko = 0
    jumlahDetergenDibeli = 0

    def bersihkan_toko(self):
        self.jumlahMembersihkanToko += 1

    def beli_detergen(self, penambahan):
        self.jumlahDetergenDibeli += penambahan

    def __str__(self) -> str:
        return super().__str__() + f"Jumlah berapa kali membersihkan toko: {self.jumlahMembersihkanToko}\nJumlah deterjen yang telah dibeli: {self.jumlahDetergenDibeli}"


# class chef yang merupakan anak dari class chef
class Chef(Karyawan):
    jumlahKueDibuat = 0
    jumlahKueDibuang = 0

    def tambahKueDibuat(self, jumlah):
        self.jumlahKueDibuat += jumlah

    def tambahKueDibuang(self, jumlah):
        self.jumlahKueDibuang += jumlah

    def __str__(self) -> str:
        return super().__str__() + f"Jumlah kue yang telah dibuat: {self.jumlahKueDibuat}\nJumlah kue yang telah dibuang: {self.jumlahKueDibuang}"


# mendefinisikan variable global untuk informasi mengenai toko Homura
listKaryawan = []
activeKaryawan = None
programBerjalan = True
jumlahDetergen = 0
jumlahKue = 0
pendapatan = 0
tanggalDibersihkan = ""


# mencetak menu toko Homura ketika tidak ada akun yang login
def menu():
    print("\nSelamat datang di Sistem Manajemen Homura\n")
    print("Apa yang ingin anda lakukan? (Tulis angka saja)")
    print("1. Register karyawan baru")
    print("2. Login")
    print("8. Status Report")
    print("9. Karyawan Report")
    print("11. Exit")
    menu = input("\nPilihan: ")
    if int(menu) == 1:
        print(
            "Format data: [username] [password] [nama] [umur] [role]")
        data = input("Input data karyawan baru: ")
        registrasi(data)
    elif int(menu) == 2:
        username = input("Username: ")
        password = input("Password: ")
        login(username, password)
    elif int(menu) == 8:
        statusReport()
    elif int(menu) == 9:
        karyawanReport()
    elif int(menu) == 11:
        print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
        exit()
    else:
        print("Pilihan tidak tersedia")


# mencetak menu kasir
def menu_kasir():
    global pendapatan
    global activeKaryawan

    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("3. Terima pembayaran")
    print("10. Logout")
    menu = input("\nPilihan: ")
    if int(menu) == 3:
        uang = int(input("Jumlah pembayaran: "))
        activeKaryawan.terima_uang(uang)
        pendapatan = pendapatan + uang
        print(f"Berhasil menerima uang sebanyak {uang}")
    elif int(menu) == 10:
        activeKaryawan = None
    else:
        print("Pilihan tidak tersedia")


# mencetak menu janitor
def menu_janitor():
    global jumlahDetergen
    global tanggalDibersihkan
    global activeKaryawan

    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("4. Bersihkan toko")
    print("5. Beli deterjen")
    print("10. Logout")
    menu = input("\nPilihan: ")
    if int(menu) == 4:
        if jumlahDetergen > 0:
            activeKaryawan.bersihkan_toko()
            tanggalDibersihkan = datetime.datetime.now()
            jumlahDetergen -= 1
        else:
            print("Diperlukan setidaknya 1 deterjen untuk melakukan pembersihan\n")
    elif int(menu) == 5:
        banyakDetergen = int(input("Jumlah pembelian deterjen: "))
        jumlahDetergen += banyakDetergen
        activeKaryawan.beli_detergen(banyakDetergen)
        print(f"Berhasil membeli {banyakDetergen} deterjen")
    elif int(menu) == 10:
        activeKaryawan = None
    else:
        print("Pilihan tidak tersedia")


def menu_chef():
    global jumlahKue
    global activeKaryawan

    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("6. Buat kue")
    print("7. Buang kue")
    print("10. Logout")
    menu = input("\nPilihan: ")
    if int(menu) == 6:
        kue = int(input("Buat berapa kue?: "))
        jumlahKue += kue
        activeKaryawan.tambahKueDibuat(kue)
        print(f"Berhasil membuat {kue} kue")
    elif int(menu) == 7:
        kue = int(input("Buang berapa kue?: "))
        if (kue > jumlahKue):
            print(
                "Tidak bisa membuang lebih banyak kue dibandingkan dengan stok kue yang ada sekarang")
        else:
            activeKaryawan.tambahKueDibuang(kue)
            jumlahKue -= kue
            print(f"Berhasil membuang {kue} kue")
    elif int(menu) == 10:
        activeKaryawan = None
    else:
        print("Pilihan tidak tersedia")
# proses registrasi dengan mengambil data-data dari string input


def registrasi(data):
    global listKaryawan
    username, password, nama, umur, role = data.split(" ")
    index = mencari_karyawan(username)
    if index == -1:
        if (role == "Kasir"):
            kasir = Kasir(username, password, nama, umur, role)
            listKaryawan.append(kasir)
        elif (role == "Janitor"):
            janitor = Janitor(username, password, nama, umur, role)
            listKaryawan.append(janitor)
        elif (role == "Chef"):
            chef = Chef(username, password, nama, umur, role)
            listKaryawan.append(chef)
        print(f"Karyawan {nama} berhasil ditambahkan")
    else:
        print(f"Karyawan {nama} sudah pernah ditambahkan")


# proses login
def login(username, password):
    global activeKaryawan
    global listKaryawan

    index = mencari_karyawan(username)
    if(index != -1):
        if(listKaryawan[index].password == password):
            activeKaryawan = listKaryawan[index]
            activeKaryawan.set_terakhir_login(datetime.datetime.now())
            print(f"Selamat datang {activeKaryawan.nama}")
        else:
            print("Username atau pasword salah\n")
    else:
        print("Username atau pasword salah\n")


# mendapatkan attribut2 dari toko Homura
def statusReport():
    global listKaryawan
    global pendapatan
    global jumlahKue
    global jumlahDetergen
    global tanggalDibersihkan

    print("===================================")
    print("STATUS TOKO HOMURA SAAT INI")
    print(f"Jumlah Karyawan: {len(listKaryawan)}")
    print(f"Jumlah Cash: {pendapatan}")
    print(f"Jumlah Kue: {jumlahKue}")
    print(f"Jumlah deterjen: {jumlahDetergen}")
    print(f"Terakhir kali dibersihkan: {tanggalDibersihkan}")
    print("===================================")


# mencetak report karyawan toko Homura
def karyawanReport():
    global listKaryawan

    for i in range(len(listKaryawan)):
        print("===================================")
        print(listKaryawan[i])
        print("===================================")


# done
def exit():
    global programBerjalan
    programBerjalan = False


def mencari_karyawan(username):
    global listKaryawan
    for i in range(len(listKaryawan)):
        if (listKaryawan[i].username == username):
            return i
    return -1


while programBerjalan == True:
    # memeriksa apakah sudah terjadi login atau belum
    if(activeKaryawan == None):
        menu()
    # jika sudah login
    else:
        # memeriksa jika yang login adalah kasir
        if(activeKaryawan.get_peran() == "Kasir"):
            menu_kasir()
        elif(activeKaryawan.get_peran() == "Janitor"):
            menu_janitor()
        elif(activeKaryawan.get_peran() == "Chef"):
            menu_chef()
